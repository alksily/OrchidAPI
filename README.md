Example API based on Orchid Framework
====

#### Requirements
- PHP >= 7.0
- Orchid Framework


#### Installation
Clone to folder on server, run `composer install`, add table `catalog` to 
your database  and change `config.default.php`.

#### Demo JWT
Key must be transfered as GET/POST value with key `apikey` or header with name `x-auth-token`.

```
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEifQ.dzH7qfQYwE42FOLkvKJWLmmb12x4SZ2hLn3qH0feqmg
```

External test: https://apitester.com/shared/checks/25dd85a2d59b4ca9999f63d77fe240bc

#### Catalog list

Request
```
http://[HOST_NAME]/catalog
```

Response
```json
[
    {
        "id": "1",
        "name": "Apple iPhone 8",
        "price": "950.00",
        "created_at": "2018-08-01 00:00:00"
    },
    {
        "id": "2",
        "name": "Apple iPhone 8 Plus",
        "price": "1000.00",
        "created_at": "2018-08-02 00:00:00"
    },
    {
        "id": "3",
        "name": "Trust Blue",
        "price": "150.00",
        "created_at": "2018-08-03 00:00:00"
    },
    {
        "id": "4",
        "name": "Trust Red",
        "price": "170.00",
        "created_at": "2018-08-04 00:00:00"
    },
    ...
]
```

 Args

Price as array (two elements)
```
price[]=100
price[]=200
```

Date as array (two elements)
```
date[]=2018-08-02
date[]=2018-08-03
```

Limit and Offset
```
offset=0
limit=30
```

Sort field and direction
```
sortField=name // or id, price, created_at
sortDirection=asc // or desc
```

#### Catalog item view by id

Request
```
http://[HOST_NAME]/catalog/2
```

Response
```json
{
    "id": "2",
    "name": "Apple iPhone 8 Plus",
    "price": "1000.00",
    "created_at": "2018-08-02 00:00:00"
}
```

#### License
The Orchid Framework is licensed under the MIT license. See [License File](LICENSE.md) for more information.
