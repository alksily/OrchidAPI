CREATE TABLE `catalog` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_price_index` (`price`),
  KEY `catalog_created_at_index` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

INSERT INTO catalog (id, name, price, created_at) VALUES (1, 'Apple iPhone 8', 950.00, '2018-08-01 00:00:00');
INSERT INTO catalog (id, name, price, created_at) VALUES (2, 'Apple iPhone 8 Plus', 1000.00, '2018-08-02 00:00:00');
INSERT INTO catalog (id, name, price, created_at) VALUES (3, 'Trust Blue', 150.00, '2018-08-03 00:00:00');
INSERT INTO catalog (id, name, price, created_at) VALUES (4, 'Trust Red', 170.00, '2018-08-04 00:00:00');