<?php

namespace Model;

use AEngine\Orchid\Model;
use Db;

class Catalog extends Model
{
    protected static $field = [
        'id'           => 0,
        'name'         => '',
        'price'        => 0.0,
        'created_at'   => 0,
    ];

    /**
     * Получение данных товаре
     *
     * @param array $data
     *
     * @return Catalog
     */
    public static function fetch(array $data = [])
    {
        $default = [
            'id'     => 0,
        ];
        $data    = array_merge($default, $data);

        $where   = [1];

        if ($data['id']) {
            $where[] = "`id` = '" . (int)$data['id'] . "'";
        }

        $where = implode(' AND ', $where);

        $result = Db::selectOne("
            SELECT
                `id`,
                `name`,
                `price`,
                `created_at`
            FROM
                `catalog`
            WHERE
                {$where}
            LIMIT 1
        ");

        return new static(is_array($result) ? $result : []);
    }

    /**
     * Добавление товара
     */
    public function save()
    {
        $this->data['created_at'] = date(\Reference\Date::DATETIME, time());

        $sth = Db::query("
            INSERT INTO `catalog` SET
                `name`        = '{$this->data['name']}',
                `price`       = '{$this->data['price']}',
                `created_at`  = '{$this->data['created_at']}'
        ");

        if ($sth->rowCount()) {
            $this->data['id'] = Db::lastInsertId();
        }

        return $sth->rowCount();
    }
}
