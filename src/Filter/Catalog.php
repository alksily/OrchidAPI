<?php

namespace Filter;

use AEngine\Orchid\Filter\AbstractFilter;
use AEngine\Orchid\Filter\TraitFilter;

class Catalog extends AbstractFilter
{
    use TraitFilter;

    /**
     * @param array $data
     *
     * @return array|bool
     */
    public static function index(array &$data = [])
    {
        $valid = new static($data);

        return $valid
            ->addGlobalRule($valid->checkValueNotEmpty(), 'Value must not be empty')
            ->option('price')
                ->addRule($valid->checkPrice(), 'Field price wrong format')
            ->option('date')
                ->addRule($valid->checkDate(), 'Field date wrong format')
            ->option('offset')
                ->addRule($valid->leadInteger())
            ->option('limit')
                ->addRule($valid->leadInteger())
            ->option('sortField')
                ->addRule($valid->checkInValues(\Reference\Catalog::SORT_FIELDS), 'sortField wrong value')
            ->option('sortDirection')
                ->addRule($valid->checkInValues(\Reference\Catalog::SORT_DIRECTION), 'sortDirection wrong value')
            ->run();
    }

    /**
     * @param array $data
     *
     * @return array|bool
     */
    public static function view(array &$data = [])
    {
        $valid = new static($data);

        return $valid
            ->addGlobalRule($valid->checkValueNotEmpty(), 'Value must not be empty')
            ->attr('id')
                ->addRule($valid->leadInteger())
            ->run();
    }

    /* ** * ** */

    /**
     * Проверка цены
     * Поле должно быть массивом и иметь два элемента в формате float
     *
     * @return \Closure
     */
    protected function checkPrice() {
        return function (&$data, $field) {
            $value = &$data[$field];

            if (is_array($value) && count($value) === 2) {
                foreach ($value as &$item) {
                    $item = (float)$item;
                }

                return true;
            }

            return false;
        };
    }

    /**
     * Проверка даты
     * Поле должно быть массивом и иметь два элемента в формате int
     *
     * @return \Closure
     */
    protected function checkDate() {
        return function (&$data, $field) {
            $value = &$data[$field];

            if (is_array($value) && count($value) === 2) {
                return true;
            }

            return false;
        };
    }
}