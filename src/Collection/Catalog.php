<?php

namespace Collection;

use AEngine\Orchid\Collection;
use Db;
use PDO;

class Catalog extends Collection
{
    protected static $model = 'Model\Catalog';

    /**
     * @var int количество строк по параметрам выборки
     */
    public $count = 0;

    public static function fetch(array $data = [])
    {
        $default = [
            'id'            => 0,
            'price'         => [],
            'date'          => [],
            'offset'        => 0,
            'limit'         => 0,
            'sortField'     => '',
            'sortDirection' => '',
        ];
        $data    = array_merge($default, $data);

        $where = [1];
        $limit = '';
        $sortField = 'id';
        $sortDirection = 'ASC';

        if ($data['id']) {
            $where[] = "`id` IN (" . implode(', ', (array)$data['id']) . ")";
        }
        if ($data['price']) {
            if ($data['price'][0]) {
                $where[] = "`price` >= '" . (float)$data['price'][0] . "'";
            }
            if ($data['price'][1]) {
                $where[] = "`price` <= '" . (float)$data['price'][1] . "'";
            }
        }
        if ($data['date']) {
            if ($data['date'][0]) {
                $where[] = "`created_at` >= '" . date(\Reference\Date::DATETIME, strtotime($data['date'][0])) . "'";
            }
            if ($data['date'][1]) {
                $where[] = "`created_at` <= '" . date(\Reference\Date::DATETIME, strtotime($data['date'][1])) . "'";
            }
        }
        if ($data['offset'] || $data['limit']) {
            $limit = "LIMIT " . max(0, (int)$data['offset']) . "," . max(0, (int)$data['limit']) . "";
        }
        if ($data['sortField']) {
            $sortField = $data['sortField'];
        }
        if ($data['sortDirection']) {
            $sortDirection = $data['sortDirection'];
        }

        $where = implode(' AND ', $where);

        $result = Db::select("
            SELECT SQL_CALC_FOUND_ROWS
                *
            FROM
                `catalog`
            WHERE
                {$where}
            ORDER BY `{$sortField}` {$sortDirection}
                {$limit}
        ");
        $count  = (int)Db::selectOne("SELECT FOUND_ROWS() AS `count`", [], 'default', PDO::FETCH_COLUMN);

        $collection        = new Catalog();
        $collection->count = $count;

        foreach ($result as $value) {
            $collection->set($value['id'], $value);
        }

        return $collection;
    }
}
