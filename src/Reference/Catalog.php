<?php

namespace Reference;

class Catalog
{
    /**
     * Возможные поля сортировки
     */
    const SORT_FIELDS = ['id', 'name', 'price', 'created_at'];

    /**
     * Возможные направления сортировки
     */
    const SORT_DIRECTION = ['asc', 'desc'];
}
