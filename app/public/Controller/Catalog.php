<?php

namespace Controller;

use AEngine\Orchid\Controller;
use AEngine\Orchid\Message\Request;
use AEngine\Orchid\Message\Response;

class Catalog extends Controller
{
    /**
     * Получение списка товаров
     *
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     */
    public function index(Request $request, Response $response)
    {
        $data = [
            'price'         => $request->getParam('price', []),
            'date'          => $request->getParam('date', []),
            'offset'        => $request->getParam('offset', 0),
            'limit'         => $request->getParam('limit', 30),
            'sortField'     => $request->getParam('sortField', ''),
            'sortDirection' => $request->getParam('sortDirection', ''),
        ];
        $result = \Filter\Catalog::index($data);

        if ($result === true) {
            $collection = \Collection\Catalog::fetch($data);

            return $response->withJson(array_values($collection->all()), 200, JSON_UNESCAPED_UNICODE);
        }

        return $response->withJson($result);
    }

    /**
     * Получение информации о товаре по его идентификатору
     *
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function view(Request $request, Response $response, array $args = [])
    {
        $data = [
            'id' => $args['id'] ?? 0,
        ];
        $result = \Filter\Catalog::view($data);

        if ($result === true) {
            $model = \Model\Catalog::fetch($data);

            if (!$model->isEmpty()) {
                return $response->withJson($model->toArray(), 200, JSON_UNESCAPED_UNICODE);
            }

            return $response->withStatus(404, 'Item not found');
        }

        return $response->withJson($result);
    }
}
