<?php

use AEngine\Orchid\Message\Request;
use AEngine\Orchid\Message\Response;
use Firebase\JWT\JWT;

use Controller\Catalog;

/**
 * Проверка наличия и правильности ключа
 *
 * @param Request  $request
 * @param Response $response
 * @param Closure  $next
 *
 * @return Response
 */
$checkAuth = function (Request $request, Response $response, $next) use ($app) {
    try {
        // получаем ключ из параметра или заголовка
        $token = $request->getParam('apikey', $request->getHeaderLine('x-auth-token'));

        // тут мы определили что юзер есть юзер
        $tokenData = (array)JWT::decode($token, $app->getSecret(), array('HS256'));

        // pre($tokenData);

        if ($tokenData['id'] == 1) {
            return $next($request, $response);
        }
    } catch (RuntimeException $e) {
        // nothing
    }

    return $response->withStatus(401, 'Unauthorized')->withJson(['auth' => 'Require token']);
};

$router = $app->router();

// catalog routes
$router->group('/catalog', function () use ($router, $checkAuth) {

    // get catalog collection
    $router->any('', [Catalog::class, 'index'])->addMiddleware($checkAuth);

    // get item by id
    $router->get('/:id', [Catalog::class, 'view'])->addMiddleware($checkAuth);

});

// fallback
$router->any('/*', function (Request $request, Response $response) {
    return $response->withStatus(400, 'Bad Request')->withJson([
        'use' => [
            '/catalog'      => 'list items',
            '/catalog/:id'  => 'item detail'
        ]
    ]);
}, -15);
